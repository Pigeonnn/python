''' - - - - - - - - - - - - - - - - - - - - - Lista 3   :   Zadanie 1  - - - - - - - - - - - - - - - - - - - - - - - '''
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fmin
from scipy.interpolate import interp1d
from sklearn.metrics import r2_score

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #pobranie danych z pliku

with open('zadanie_3_zbior_cukrzyca.txt') as f:
    array = []
    for line in f:
        array.append([int(x) for x in line.split()])

print(array)
time = []
glucose = []
insuline = []

for list in array:
    time.append(list[0])
    glucose.append(list[1])
    insuline.append(list[2])

for i in range(len(time)):
    print(time[i],glucose[i],insuline[i])

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #optymalizacja parametrów

def model(x,t,parametres):
    k1 = parametres[0]
    k2 = parametres[1]
    k3 = parametres[2]
    Gt = x[0]
    Xt = x[1]
    Gb = 92
    Ib = 11
    dGdt = k1 * (Gb-Gt) - Xt * Gt
    dXdt = k2 * (z(t)-Ib) - k3 * Xt
    return [dGdt,dXdt]

y0=[92,0]
t = np.linspace(0,182,2184)
z = interp1d(time,insuline,kind='linear',fill_value='extrapolate')                                                      #interpolacja insuliny

def kryterium(parametres):
    y = odeint(model,y0,t,args=(parametres,))
    y2 = y[:,0]
    suma = 0
    odpowiedniki = {}                                                                                                   #słownik ma zawierać odpowiedniki wyjść z pliku
    timelist = []                                                                                                       #tzn. te wartości ode, które były liczone dla jednostek
    for x in t:                                                                                                         #czasowych w liście danych "time" .
        timelist.append(x)
    for i in range(len(timelist)):
        odpowiedniki[timelist[i]]=y2[i]
    time_values = []
    model_values = []
    for x in time:
        for y in odpowiedniki.keys():
            if abs(x-y)<0.042:                                                                                          #tu- 0.036 -swojego rodzaju 'epsilon' w wybieraniu odpowiednich danych
                time_values.append(int(round(y)))                                                                       #czasowych do słownika. Będzie się zmieniał w zależności
                model_values.append(odpowiedniki[y])                                                                    #od ilości jednostek czasowych w linspace
    #print(time_values)
    #print(time)                                                                                                        #te printy pomagają znaleźć odpowiedni epsilon, time i time_values
    #print(model_values)                                                                                                #z dobrym epsilonem będą sobie równe i tego zawsze szukamy
    for i in range(len(glucose)):
        #print(glucose[i],model_values[i])
        suma += (glucose[i]-model_values[i])**2    #liczenie kryterium
    print(suma)
    #print(r2_score(glucose, model_values))
    return suma

x0 = [1, 1, 1]
optimum = fmin(kryterium,x0)                                                                                            #optymalizacja
print(optimum)

y3 = odeint(model,y0,t,args=(optimum,))

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #rysowanie
plt.figure("Glukoza")
plt.plot(time,glucose,c='g')
plt.plot(t,y3[:,0],c='r')
plt.xlabel("Czas(min)")
plt.ylabel("Poziom glukozy (mg/dl)")
plt.figure("Insulina śródmiąższowa ")
plt.plot(t,y3[:,1],c='b')
plt.xlabel("Czas(min)")
plt.ylabel("Wartość insuliny śródmiąższowej (mU/ml)")
plt.show()
