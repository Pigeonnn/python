import matplotlib.pyplot as plt
import csv

def value(time):
    string = time[0]
    result = (int(string[0]))*3600+(int(string[2:4]))*60+(int(string[5:7]))+(int(string[8:]))*0.001
    return result
def valuelap(lap):
    integer = lap[0]
    return integer

with open("F1.csv","r") as f:
    reader = csv.reader(f)
    csv_list = list(reader)

tracklen = [5303,5412,5451,6006,4664]
new_laps = [58,57,56,51,66]
names = []
times = []
laps = []
scores = []
bests = []

for x in csv_list:
    if len(x) > 5:
        if x[2] not in names:
            names.append(x[2])
for x in names:
    time = []
    lap = []
    for y in csv_list:
        for z in y:
            if x == z:
                time.append(y[-1:])
                lap_mirror = y[-2:-1]
                lap.append(int(lap_mirror[0]))
    times.append(time)
    laps.append(lap)
for x in times:
    for i in range(0,len(x)):
        x[i]=value(x[i])
for x in times:
  driver = []
  driver += x
  scores.append(driver)
best = 10000
for i in range(0,5):
  for j in range(0,20):
    if (times[j][i] < best) and (laps[j][i] == new_laps[i]):
      best = times[j][i]
  bests.append(best)
  best = 10000

print(bests)

for i in range(0,len(times)):
    for j in range(0,len(scores[0])):
        scores[i][j] = (times[i][j]/laps[i][j]/tracklen[j])
        control = times[i][j]/bests[j]
        if control < 1:
          timescore = None
          scores[i][j] = None
        else:
          timescore = times[i][j]
          scores[i][j] =-1*(timescore/laps[i][j]/tracklen[j])
x = [1,2,3,4,5]

plt.figure(num=None, figsize=(20,10), dpi=100, facecolor='w', edgecolor='k')
for i in range(0,len(names)):
    y = scores[i]#104
    plt.plot(x,y,label=names[i])
    plt.legend(loc='lower left')
plt.show()