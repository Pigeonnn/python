#    F1  Statistics scraper    |``


#    all rights reserved © ( ͡° ͜ʖ ͡°)
#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  neccesary libraries..
import numpy as np
from bs4 import BeautifulSoup as bs
import matplotlib.pyplot as plt
import requests as r
import selenium as s
import csv

races_num = 5                                                                                           # # # # # # # # update

#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -   first scrap-main site..

page = r.get("https://www.fia.com/events/fia-formula-one-world-championship/season-2019/formula-one")
soup = bs(page.content, 'html.parser')

href= []
#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  getting races links..
races = soup.select("div.event a")
for race in races:
    #print(race.attrs)
    href.append((race.attrs["href"]))

#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -    getting race clasification..

timestables = []
for url in href:
    link = 'https://www.fia.com'+url
    new_page = r.get(link)
    new_soup = bs(new_page.content, 'html.parser')
    new_href = []
    feat = new_soup.select("div.side-menu-block-wrapper a")
    for link in feat:
        new_href.append((link.attrs["href"]))
    timestables.append(new_href[5])
#print(timestables)

#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -   getting proper lap times..

final_new_final = []
for i in range(0, races_num):
    url = timestables[i]
    link = 'https://www.fia.com' + url
    new_page_final = r.get(link)
    new_soup_final = bs(new_page_final.content, 'html.parser')
    result = new_soup_final.select("table.sticky-enabled")
    result_time = [rt.get_text(separator=' ') for rt in result]

#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -   making csv file..

    for x in result_time:
        result_time_mirror = ''
        for i in range(0, len(x) - 1):
            if (x[i] == ' ') and (x[i + 1] == ' '):
                result_time_mirror += ''
            elif (x[i + 1] == ''):
                result_time_mirror += ''
            elif (x[i - 1] == ''):
                result_time_mirror += ''
            elif (x[i]) == ' ':
                result_time_mirror += ' '
            else:
                result_time_mirror += x[i]

        final_result = []
        temp = []
        word = ''

        for i in range(0, len(result_time_mirror)):
            if result_time_mirror[i] == ' ':
                final_result.append(word)
                word = ''
            else:
                word += result_time_mirror[i]

        for x in final_result:
            if x != '\n':
                temp.append(x)
            else:
                final_new_final.append(temp)
                temp = []

with open("F1.csv", "w", newline='') as f:
    writer = csv.writer(f)
    writer.writerows(final_new_final)

# Voila!