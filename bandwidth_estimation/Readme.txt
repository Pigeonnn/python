The purpose of this project was designing a tool which will be able to predict certain
network parameters, based on previously performed measurements. In this case the target of
research is all-day network speed.

Main elements of this system are measuring device and predicting device which can
be used by a user through an interface. System can be used on every computer unit which has
Python interpreter installed.



Installation process:

-Extract files to desired location
-Run install.sh

Then launch:
-execute SpeedCheck.py

Enjoy!