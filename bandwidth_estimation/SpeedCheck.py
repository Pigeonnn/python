#!/usr/bin/env python -W ignore::DeprecationWarning
import csv
import app
import helloworld
import os
import sys
import re
import time
import shutil


def slow_write(str):
    for x in str:
        sys.stdout.write(x)
        sys.stdout.flush()
        time.sleep(0.005)
def very_slow_write(str):
    for x in str:
        sys.stdout.write(x)
        sys.stdout.flush()
        time.sleep(0.1)
def show_chart(node):
    while 1:
        os.system('cls')
        fun_done = 1
        action_3 = input('Press:\n 1 - to see speed chart \n 2 - to ask for specified hour \n q - for quit\n')
        if action_3 == '1':
            app.show_chart(node)
        elif action_3 == '2':
            pattern = '^([0-1][0-9]|[2][0-3]):([0-5][0-9])$'
            spec_time = input('Specify desired time  ( format hh:mm ) \n')
            reg_check = re.match(pattern,spec_time)
            if reg_check:
                os.system('cls')
                print('\nDownload speed at that time  -  ',app.give_value(node,spec_time),' Mbps')
                input('\npress any key to continue .. ')
            else:
                print('You typed wrong format!')
                time.sleep(2)
        elif action_3 =='q':
            return fun_done
        else:
            os.system('cls')
            slow_write('You did not choose correct letter.')
            time.sleep(1)
            continue
def getnextnode():
    if len(os.listdir(os.getcwd()+'\\nodes'))==0:
        return 1
    else:
        files = os.listdir(os.getcwd()+'\\nodes')
        files_mirror = []
        for x in files:
            files_mirror.append(int(x))
        files_mirror.sort()
        last = files_mirror[-1]
        last_int = int(last)
        next = last_int + 1
        return next
def renumber(node_number):
    dir_list = os.listdir(os.getcwd() + '\\nodes')
    dir_list_mirror = []
    for x in dir_list:
        dir_list_mirror.append(int(x))
    dir_list_mirror.sort()
    for filename in dir_list_mirror:
        if filename > int(node_number):
            filename_2 = int(filename)
            filename_2 = filename_2 - 1
            os.rename(os.getcwd() +'\\nodes\\'+str(filename)+'\\'+str(filename)+'.txt',os.getcwd() +'\\nodes\\'+str(filename)+'\\'+str(filename_2)+'.txt')
            os.rename(os.getcwd() +'\\nodes\\'+str(filename),os.getcwd() +'\\nodes\\'+str(filename_2))

intro = 0
done = 0
node = 0
node_name = '|not chosen|'

while True:
    next_node = getnextnode()
    fun_done = 0
    if node != 0:
        f = open("nodes\\" + node + "\\" + node +".txt", "r")
        node_name = f.read()
        f.close()
        node_name = node_name[:-1]
    os.system('cls')
    if intro == 0:
        slow_write('Welcome to SpeedCheck!\n Open Source Software,  Radosław Malus 2k20\n\n')
        intro = 1
    print('Press:\n 1 - to ask for interned speed          |' + '       current node ~ ' + node_name + '\n 2 - to update predictions              |\n 3 - for speedtest                      |\n 4 - choose node                        |\n 5 - to create new node                 |\n 6 - to delete node                     |\n q - to leave                           |\n')
    action = input()




    if action == '1':
        os.system('cls')
        if node == 0:
            slow_write('You must choose node first.')
            time.sleep(2)
            fun_done = 1
        else:
            filenames = os.listdir(os.getcwd() + '\\nodes\\'+node)
            if 'predictions.txt' in filenames:
                fun_done = show_chart(node)
            else:
                slow_write('There are no predictions for this node!')
                time.sleep(2)
                fun_done = 1





    if action == '2':
            if node == 0:
                os.system('cls')
                slow_write('You must choose node first.')
                time.sleep(2)
            else:
                filenames = os.listdir(os.getcwd() + '\\nodes\\' + node)
                if 'speed.txt' in filenames:
                    b = app.checkhours(node)
                    if b == True:
                        os.system('cls')
                        slow_write('\nNote: if you want ARIMA model to work correctly, it is recommended to make 24h-scope measurements.\nOtherwise - results may be unsatisfying.')
                        time.sleep(6)
                    while 1:
                        os.system('cls')
                        action_4 = input('Warning - this process can take more than few minutes. Press:\n 1 - to start model training \n q - quit\n')
                        if action_4 == '1':
                            os.system('cls')
                            slow_write('ARIMA model training procedure started.. \n\n')
                            time.sleep(2)
                            app.arima(node)
                            os.system('cls')
                            slow_write('\nTraining done! Data updated. ')
                            input()
                            break
                        elif action_4 =='q':
                            break
                        else:
                            os.system('cls')
                            slow_write('You did not choose correct letter.')
                            time.sleep(1)
                            continue
                else:
                    os.system('cls')
                    slow_write('You need to make measurements first! ')
                    time.sleep(2)





    elif action == '3':
        if node == 0:
            os.system('cls')
            slow_write('You must choose node first.')
            time.sleep(2)
        else:
            while 1:
                os.system('cls')
                action_2 = input('Press:\n 1 - to schedule measurements\n 2 - to do one-time run\n')
                if action_2 =='1':
                    os.system('cls')
                    hours = int(input('How many hours forward you want to analyse?\n'))
                    os.system('cls')
                    measurements = int(input("How many measurements you want to do at that time?\n"))
                    os.system('cls')
                    for i in range(0,measurements):
                        time_break = hours * 3600 / measurements
                        time_break = round(time_break,3)
                        os.system('cls')
                        print('Speedtest is on for url = ', node_name)
                        time.sleep(2)
                        time_elapsed,speed = helloworld.main(node_name,node)
                        time_break = time_break - time_elapsed - 2
                        seconds = time.time() + time_break
                        local_time = time.ctime(seconds)
                        print("Speedtest complete!\n\nSpeed          - ", str(speed), 'Mbps')
                        print("Time elapsed   - ", str(time_elapsed))
                        print('\n\nNext measurement :', local_time[4:-5])
                        print('\nMeasurements left :', measurements-i-1)
                        print('\nPress CTRL + C to leave program.')
                        time.sleep(time_break)
                    break

                elif action_2 =='2':
                    os.system('cls')
                    print('Speedtest is on for url = ',node_name)
                    time.sleep(2)
                    time_elapsed, speed = helloworld.main(node_name, node)
                    print("Speedtest complete!\n\nSpeed          - ", str(speed), 'Mbps')
                    print("Time elapsed   - ", str(time_elapsed))
                    input()
                    break

                else:
                    os.system('cls')
                    slow_write('You did not choose correct letter.\n')
                    time.sleep(1)
                    continue



    elif action == '4':
        os.system('cls')
        print('\n 0 - clean your choice\n')
        for filename in os.listdir(os.getcwd() + '\\nodes'):
            filename_mirror = str(filename)
            f = open("nodes\\" + filename + "\\" + filename + ".txt", "r")
            mirror_node_name = f.read()
            f.close()
            mirror_node_name = mirror_node_name[:-1]
            print('', filename_mirror, '-', mirror_node_name)
        print('\n which you select?')
        node = input()
        if node == '0':
            node = int(node)
            node_name = '|not chosen|'




    elif action == '5':
        os.system('cls')
        slow_write('Specify node\'s http sourced file..\n\n')
        control = input()
        node_site = control
        path = "nodes\\"+str(next_node)
        os.mkdir(path)
        with open("nodes\\"+str(next_node)+"\\"+str(next_node)+".txt", 'a') as p:
            print(node_site, file=p)
            node = str(next_node)
        fun_done = 1




    elif action == '6':
        if node != 0:
            os.system('cls')
            slow_write('You can\'t delete node if any is currently chosen!')
            time.sleep(2)
            fun_done = 1
        else:
            if len(os.listdir(os.getcwd() + '\\nodes')) == 0:
                os.system('cls')
                slow_write('There are no nodes created.')
                time.sleep(2)
                fun_done = 1
            else:
                while 1:
                    os.system('cls')
                    for filename in os.listdir(os.getcwd() + '\\nodes'):
                        filename_mirror = str(filename)
                        f = open("nodes\\" + filename + "\\" + filename + ".txt", "r")
                        mirror_node_name = f.read()
                        f.close()
                        mirror_node_name = mirror_node_name[:-1]
                        print('', filename_mirror, '-', mirror_node_name)
                    slow_write('\n Specify node\'s number or press q - to go back.\n ')
                    control_2 = input()
                    if control_2 == 'q':
                        fun_done = 1
                        break
                    elif control_2 in os.listdir(os.getcwd() + '\\nodes'):
                        node_number = control_2
                        path = "nodes\\" + str(node_number)
                        shutil.rmtree(path)
                        renumber(node_number)
                        fun_done = 1
                        break
                    else:
                        os.system('cls')
                        slow_write('You did not choose correct letter.\n')
                        time.sleep(1)
                        continue




    elif action == 'q':
        while 1:
            slow_write('Are you sure?  (Y/N)\n')
            action_2 = input()
            if action_2 == 'y' or action_2 =='Y':
                os.system('cls')
                done = 1
                break
            elif action_2 =='N' or action_2 =='n':
                break
            else:
                os.system('cls')
                slow_write('You did not choose correct letter.\n')
                time.sleep(1)
                continue
    else:
        if fun_done == 1:
            continue
        else:
            os.system('cls')
            slow_write('You did not choose correct letter.')
            time.sleep(1)
            continue
    if done == 1:
        break