import csv
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
import time

def measuremenets(node):
    def time_sort(sub_list):
        sub_list.sort(key=lambda x: x[1])
    with open("nodes\\"+node+"\speed.txt","r") as f:
        reader = csv.reader(f)
        csv_list = list(reader)

    for x in csv_list:
        if len(x[1]) == 4:
            y = '0'+x[1]
            x[1] = y

    values = []
    timeplot = []
    data_set = []
    average = -1
    counter = 1


    time_sort(csv_list)
    lenn = len(csv_list)

    for i in range(0,lenn-1):
        if average == -1:
            average = float(csv_list[i][2])
        if csv_list[i][1] == csv_list[i+1][1]:
            counter += 1
            sample = csv_list[i+1][2]
            sample_r = round(float(sample),6)
            average += sample_r
            if i == (lenn-2):
                values.append(average/counter)
                timeplot.append(csv_list[i][1])
        else:
            values.append(average/counter)
            average = float(csv_list[i+1][2])
            counter = 1
            timeplot.append(csv_list[i][1])
            if i == (lenn-2):
                values.append(average/counter)
                timeplot.append(csv_list[i][1])

    for i in range(0,len(timeplot)):
        data_set.append([timeplot[i][0:5],values[i]])
    return data_set
def day_cells():
    counter = 0
    time = []
    for i in range(0,24):
        minuter = 0
        for j in range(0,4):
            if i < 10:
                if minuter == 0:
                    time.append('0'+str(i)+':' + str(minuter)+'0')
                else:
                    time.append('0' + str(i) + ':' + str(minuter))
            else:
                if minuter == 0:
                    time.append(str(i) + ':' + str(minuter) + '0')
                else:
                    time.append(str(i) + ':' + str(minuter))
            minuter += 15
            counter += 1
            if counter == 4:
                break
    return time
def merge(mes,day,node):
    values = []
    timeplot = []
    mirror = []
    set_up = []
    set_up_2 = []

    average = -1
    counter = 1

    for x in mes:
        time_str = x[0][0:2] + x[0][3:5]
        time_int = int(time_str)
        mirror.append([time_int,x[1]])
    for y in day:
        time_str = y[0:2]+y[3:5]
        time_int = int(time_str)
        if time_str[2:5] == '00':
            if time_str[0:2] =='00':
                left = 2353
                right = 8
                time_range_1 = list(range(left, left+6))
                time_range_2 = list(range(right -8, right))
                time_range = time_range_1 + time_range_2
            else:
                left = time_int - 7
                right = time_int + 8
                time_range_1 = list(range(left - 40, right - 48))
                time_range_2 = list(range(left + 7, right))
                time_range = time_range_1 + time_range_2
        else:
            left = time_int - 7
            right = time_int + 8
            time_range = list(range(left, right))
        for z in mirror:
            if z[0] in time_range:
                set_up.append([y,z[1]])

    for i in range(0, len(set_up) - 1):
        if i == 0:
            average = float(set_up[i][1])
        if set_up[i][0] == set_up[i + 1][0]:
            counter += 1
            sample = set_up[i + 1][1]
            sample_r = round(float(sample), 6)
            average += sample_r
        else:
            values.append(average/counter)
            average = float(set_up[i + 1][1])
            counter = 1
            timeplot.append(set_up[i][0])
        if i == len(set_up) - 2:
            values.append(average / counter)
            timeplot.append(set_up[i+1][0])

    for i in range(0, len(timeplot)):
        set_up_2.append([timeplot[i][0:5], values[i]])

    return timeplot,values,set_up_2
def arima(node):
    def time_sort(sub_list):
        sub_list.sort(key=lambda x: x[2])
    mes = measuremenets(node)
    day = day_cells()
    timeplot,values,mir_list = merge(mes,day,node)

    set_up_dict = {mir_list[i][0]: mir_list[i][1] for i in range(0, len(mir_list))}
    new_list = []
    if len(mir_list) == 1:
        for x in day:
            set_2 = [x,mir_list[0][1]]
            new_list.append(set_2)
        set_up_dict_2 = {new_list[i][0]: new_list[i][1] for i in range(0, len(new_list))}
        predictions = list(set_up_dict_2.values())
        hours = list(set_up_dict_2.keys())
    else:
        average = 0
        for x in values:
            average += x
        average = average/len(values)
        for i in range(0,len(day)):
            if day[i] in timeplot:
                set_3 = [day[i], set_up_dict[day[i]]]
            else:
                set_3 = [day[i],average]
            new_list.append(set_3)
        set_up_dict_2 = {new_list[i][0]: new_list[i][1] for i in range(0, len(new_list))}
        train = list(set_up_dict_2.values())
        hours = list(set_up_dict_2.keys())

        param = 28
        while 1:
            try:
                model_arima = ARIMA(train, order=(param,0,1))
                model_arima_fit = model_arima.fit(maxiter=50)
                break
            except Exception:
                param -= 1
                if param == 0:
                    print('\nNeed more measurements for ARIMA training! ')
                    time.sleep(2)
                    break

        predictions = model_arima_fit.forecast(steps=96)[0]
        time.sleep(2)
    set_up_4 = []

    for i in range(0,len(hours)):
        particle = hours[i],predictions[i]
        set_up_4.append(particle)

    open("nodes\\"+node+"\predictions.txt", 'w').close()

    with open("nodes\\"+node+"\predictions.txt", 'a') as p:
        for i in range(0,len(hours)):
            string_1 = ''
            string_1 += str(hours[i]) + ','+ str(predictions[i])
            print(string_1, file=p)
    print('\n'*10)
    return(predictions)
def show_chart(node):
    with open("nodes\\"+node+"\predictions.txt","r") as f:
        reader = csv.reader(f)
        csv_list = list(reader)
    predictions_mirror = []
    time_mirror = []
    for x in csv_list:
        time_mirror.append(x[0])
        rounded = round(float(x[1]),2)
        predictions_mirror.append(rounded)
    ax = plt.figure(figsize=(16,5))
    ax = plt.subplot()
    ax.yaxis.set_major_locator(plt.MaxNLocator(10))
    ax.xaxis.set_major_locator(plt.MaxNLocator(12))
    ax.plot(time_mirror,predictions_mirror, c='r')
    plt.show()
def give_value(node,spec_time):
    with open("nodes\\" + node + "\predictions.txt", "r") as f:
        reader = csv.reader(f)
        csv_list = list(reader)
        spec_time_mirror = spec_time[0:2] + spec_time[3:5]
        spec_time_int = int(spec_time_mirror)
        for x in csv_list:
            y = x[0]
            time_str = y[0:2]+y[3:5]
            time_int = int(time_str)
            if time_str[2:5] == '00':
                if time_str[0:2] == '00':
                    left = 2353
                    right = 8
                    time_range_1 = list(range(left, left + 6))
                    time_range_2 = list(range(right - 8, right))
                    time_range = time_range_1 + time_range_2
                else:
                    left = time_int - 7
                    right = time_int + 8
                    time_range_1 = list(range(left - 40, right - 48))
                    time_range_2 = list(range(left + 7, right))
                    time_range = time_range_1 + time_range_2
            else:
                left = time_int - 7
                right = time_int + 8
                time_range = list(range(left, right))

            if spec_time_int in time_range:
                result = x[1]
                return result
def checkhours(node):
    with open("nodes\\"+node+"\speed.txt","r") as f:
        reader = csv.reader(f)
        csv_list = list(reader)
        timetable = []
        day = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23']
        for x in csv_list:
            if x[1][1] == ':':
                timetable.append(x[1][0])
            else:
                timetable.append(x[1][0:2])
        timetable = set(timetable)
        for x in day:
            if x not in timetable:
                return True
        return False