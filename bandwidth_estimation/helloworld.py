import os
import requests
import sys
import time
import datetime

def downloadFile(url, directory) :
    localFilename = url.split('/')[-1]
    print("\n")
    now = datetime.datetime.now()
    if now.minute < 10:
        minutee = "0"+str(now.minute)
    else:
        minutee = now.minute

    curr_time = str(now.weekday())+','+str(now.hour)+':'+str(minutee)
    with open(directory + '/' + localFilename, 'wb') as f:
        start = time.clock()
        r = requests.get(url, stream=True)
        total_length = r.headers.get('content-length')
        dl = 0
        if total_length is None: # no content length header
           f.write(r.content)
        else:
            with open(directory+'\speed.txt', 'a') as p:
                for chunk in r.iter_content(1024):
                  dl += len(chunk)
                  f.write(chunk)
                  done = 50 * dl / int(total_length)
                  done = int(done)
                  speed = (dl//(time.clock() - start))/131072
                  if done > 48:
                    print(curr_time + ',' + str(speed), file=p)
                  number = (dl//(time.clock() - start))/131072
                  speed_print = round(number, 3)
                  sys.stdout.write("\r[%s%s] %s Mbps" % ('=' * done, ' ' * (50-done), speed_print))
    return (time.clock() - start,speed)

def main(url, node) :
    directory =  os.getcwd()+'\\nodes\\'+node
    time_elapsed,speed = downloadFile(url,directory)
    os.system('cls')
    time_elapsed = round(time_elapsed,2)
    speed = round(speed,2)
    return time_elapsed,speed