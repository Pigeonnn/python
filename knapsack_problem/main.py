'''  ClearCode Recruitment Task    |``  '''


def calculate(usb_size,memes):
    usb_size *= 1024
    how_much_worth_it = []
    c = 0
    backup = 0
    highest = 0
    highest_for = 0
    names = set()
    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  value to weight ratio..
    for i in range(0,len(memes)):
        how_much_worth_it.append([memes[i][2]/memes[i][1],i])

    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -   main loop, gathering memes..
    for k in range(len(how_much_worth_it)-1,-1,-1):
        how_much_worth_it.sort(key=lambda x: x[0])
        weigh_sum = 0
        val_sum = 0
        which = []
        for x in range(len(how_much_worth_it)-1,-1,-1):
            weight = memes[how_much_worth_it[x][1]][1]
            val = memes[how_much_worth_it[x][1]][2]
            if weigh_sum+weight > usb_size:
                break
            else:
                weigh_sum += weight
                val_sum += val
                which.append(how_much_worth_it[x][1])
        #  -  -  -  -  -  -  -  - leaving specific memes for different combination..
        if backup != 0:
            how_much_worth_it.append(backup)
            out = len(how_much_worth_it) - 1 - c
            backup = how_much_worth_it[out]
            del how_much_worth_it[out]
        else:
            backup = how_much_worth_it[-1]
            del how_much_worth_it[-1]
        c += 1
        #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  checking combination..
        if val_sum > highest:
            highest = val_sum
            highest_for = [which,weigh_sum]
    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  output..
    for y in highest_for[0]:
        names.add(memes[y][0])

    return (highest,names)