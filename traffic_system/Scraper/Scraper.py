import pika
import time
import requests


def getImages(link,highwayID,amountOfImages,timeBetweenRequest):

    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit', "5672", "/", pika.PlainCredentials('guest', 'guest')))
    channel = connection.channel()

    channel.queue_declare(queue='images')

    for i in range(0,amountOfImages):
        result = requests.get(link).content
        result = result+highwayID.encode()
        print(result+highwayID.encode());
        print("Image "+str(i+1) +" has been received from WebCam!")
        time.sleep(timeBetweenRequest)
        channel.basic_publish(exchange='',routing_key='images',body=result,properties=pika.BasicProperties(delivery_mode=2,))
        print('Message has been sent to queue.')
        time.sleep(2)

    connection.close()

getImages("http://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=automotor&wcsid=A010_005,700_0_00003136&ssid=268268d9-34b1-490b-9d04-2f8734e4297f&time=0.016767896051189668","A02_04",500,5)





