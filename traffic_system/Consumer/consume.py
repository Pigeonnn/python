import os
import pika
import time
import io
import requests
import simplejson as json

from imageai.Detection import ObjectDetection

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit', "5672", "/", pika.PlainCredentials('guest', 'guest')))
channel = connection.channel()
channel.queue_declare(queue='images')

index = 1;

def network_ai(image):
    
    execution_path = os.getcwd()
    
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath( os.path.join(execution_path , "resnet50_coco_best_v2.0.1.h5"))
    detector.loadModel()
    detections = detector.detectObjectsFromImage(input_type="stream",input_image=image,output_image_path=os.path.join(execution_path , "image.jpg"))

    print("Zdjęcie: ", image, "Ilość znalezionych obiektów: ", len(detections))

    return(detections)

def send_information_photo(detections_info, highwayId):
    url = "http://server:5000/tasks"
    data = {'id_highway': highwayId,
            'all_info': detections_info}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url, data=json.dumps(data), headers=headers)

    print("Photo info send from" + highwayId + "highway!")

def callback(ch, method, properties, body):
    global index
    highwayId = str(body)[-7:-1]
    #image_location = highwayId+"_image" + str(index) + ".jpg"
    #image_saver = open(image_location, "wb")
    #image_saver.write(body)
    #image_saver.close()
    print("Image received from Camera: " + highwayId)
    print(str(index) + ". Message received!")
    index += 1
    image_prepare_to_network = io.BytesIO(body)
    ch.basic_ack(delivery_tag=method.delivery_tag)
    detections_info = network_ai(image_prepare_to_network)
    print(detections_info)
    send_information_photo(detections_info, highwayId)
    time.sleep(1)
    
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='images', on_message_callback=callback)
channel.start_consuming()



