INSTRUKCJE STWORZONE PRZEZ MIKIM 

Sprawa wygl�da tak, aby aktualnie wszystko uruchomi� i przetestowa�:

UWAGA! Przed przyst�pieniem do dzia�a� skopiuj plik resnet50_coco_best_v2.0.1 do folderu z projektem nie wrzucam na repo bo du�o wa�y.
1. W lokalizacji projektu w cmd wpisujemy "python serwer.py" - uruchomilismy serwer i zostawiamy tak okno cmd
2. W innym oknie uruchamiamy docker-compose up
3. Po chwili pod adresem http://localhost:15672/#/ powinni�my mie� dost�pnego rabita
4. Nast�pnie uruchamiamy kolejne okno cmd w lokalizacji projektu i wpisujemy "python consume.py" - uruchomilismy konsumenta
5. Pod adresem http://localhost:5000/tasks powinni�my mie� wy�wietlone kolejne odebrane elementy z api.

SPIS ADRES�W:
http://localhost:5000/ - tutaj b�dzie znajdowa� si� interfejs
http://localhost:5000/tasks - tutaj znajduj� si� aktualnie odebrane dane w api 
http://localhost:5000/tasks/<jakie� id z api> - konkretny element z api o danym id

http://localhost:15672/ - rabit


