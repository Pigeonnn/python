from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

#this is one example
tasks = [
    {
        'id': 1,
		'id_highway': 'xxx',
		'all_info': 'x'
    }
]

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})
	
@app.route('/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})
	
@app.route('/tasks', methods=['POST'])
def create_task():
    if not request.json or not 'id_highway' in request.json or not 'all_info' in request.json:
        print('abort(400)')
    task = {
        'id': tasks[-1]['id'] + 1,
        'id_highway': request.json['id_highway'],
        'all_info': request.json['all_info']
    }
    tasks.append(task)
    return jsonify({'task': task}), 201

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)