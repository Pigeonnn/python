import keras
import numpy as np
from sklearn.model_selection import train_test_split
#import pandas as pd
#import tensorflow as tf
#from keras.callbacks import TensorBoard
from sklearn.utils import shuffle
from sklearn import metrics
import time
import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd

#NAME = "Neural_network_nose_{}".format(int(time.time()))
#tensorboard = TensorBoard(logdir='logs/()'.format(NAME))

seed = 10
np.random.seed(seed)

dataset = np.loadtxt("new_dataset_2.csv",delimiter=',',skiprows=1)
dataset = shuffle(dataset)

X = dataset[:,2:]
Y = dataset[:,1]
(X_train,X_test,Y_train,Y_test) = train_test_split(X, Y, test_size=0.15, random_state=seed)
input_shape = (13,)

model = keras.models.Sequential()
model.add(keras.layers.InputLayer(input_shape))
model.add(keras.layers.core.Dense(128, activation='relu'))
model.add(keras.layers.core.Dense(128, activation='relu'))
model.add(keras.layers.core.Dense(4, activation='sigmoid'))

model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',metrics=['accuracy'])
history = model.fit(X_train,Y_train,validation_split=0.25,epochs=50,batch_size=32)

val_loss, val_acc = model.evaluate(X_test,Y_test)
print('\nCost = ',val_loss,'\nAccuracy = ',val_acc,' \n')

plt.figure("1")
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train','Test'], loc='upper left')

plt.figure("2")
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train','Test'], loc='upper left')
plt.show()

predictions = model.predict(X_test)
predictions2=[]
for x in predictions:
    y = list(x)
    var = max(y)
    index = y.index(var)
    predictions2.append(index)


matrix = metrics.confusion_matrix(Y_test,predictions2)
df_cm = pd.DataFrame(matrix, index = [i for i in ['Spoiled','Acceptable','Good','Excellent']],
                  columns = [i for i in ['Spoiled','Acceptable','Good','Excellent']])

plt.figure(figsize = (10,7))
plt.title("Confusion matrix")
sn.heatmap(df_cm, annot=True, fmt='g')
plt.show()