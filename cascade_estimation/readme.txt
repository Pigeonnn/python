Celem zadania jest identyfikacja obiektu bedacego kaskada polaczonych zbiornikow z odplywem:

Zdjecie systemu: 
https://i.imgur.com/ayuPFHP.png

Model systemu jest okreslony wzorem: 
https://i.imgur.com/ZkU8JmL.png

Rozwiazaniem jest zaimplementowanie algorytmu estymacji parametrow tego modelu. Do weryfikacji 
maja s�uzy� zbiory zawarte w plikach:
zadanie3zbior1 - zbi�r treningowy 
zadanie3zbior2 - zbi�r testowy


Aby rozwiazac ten problem w pliku ObjectCascade_estimation zaimplementowalem algorytm wykorzystujacy
solver rownan rozniczkowych oraz optymalizator znajdujacy minimum funkcji. Optymalizator
opiera sie na metodzie gradientowego spadku. 

Schemat dzialania algorytmu:

Solver minimalizujacy wstawia wygenerowane wartosci do funkcji kryterium -> 
funkcja kryterium przekazuje wartosci do solvera rownan rozniczkowych -> 
solver estymuje wyniki na podstawie parametrow ->
wyliczanie kryterium jakosci na podstawie wynikow ->
zwrot wartosci kryterium do solvera minimalizujacego ->

--> powtorz az do otrzymania satysfakcjonujacego wyniku

Po zwroceniu wartosci przez solver nastepuje wizaulizacja rozwiazania.

