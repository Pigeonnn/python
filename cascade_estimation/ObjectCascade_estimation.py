''' - - - - - - - - - - - - - - - - - Zadanie identyfikacji kaskady zbiorników - - - - - - - - - - - - - - - - - - - '''
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fmin
from scipy.interpolate import interp1d
import scipy.io
from sklearn.metrics import r2_score

list_u1 = []
list_y1 = []
list_u2 = []
list_y2 = []

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #wpisywanie danych do tablic

file = scipy.io.loadmat('zadanie_3_zbior_1.mat')
#print(file)
data_arrayu = file['u']
data_arrayy = file['y']
arrayu = data_arrayu[0]
arrayy = data_arrayy[0]
for x,y in zip(arrayu,arrayy):
    list_u1.append(x)
    list_y1.append(y)


file = scipy.io.loadmat('zadanie_3_zbior_2.mat')
#print(file)
data_arrayu = file['u']
data_arrayy = file['y']
arrayu = data_arrayu[0]
arrayy = data_arrayy[0]
for x,y in zip(arrayu,arrayy):
    list_u2.append(x)
    list_y2.append(y)


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #wypisanie zaimportowanych danych

print('     u1      y1\n')
for i in range(len(list_u1)):
    print (i+1,'. ','{0:.4f}'.format(list_u1[i]),'{0:.4f}'.format(list_y1[i]))
print('\n     u2      y2\n')
for i in range(len(list_u2)):
    print (i+1,'. ','{0:.4f}'.format(list_u2[i]),'{0:.4f}'.format(list_y2[i]))

'''                                                                                                                     oznaczenia
h1(t) - poziom cieczy w zbiorniku 1;
h2(t) - poziom cieczy w zbiorniku 2;
u(t) - wielkość sterująca (zasilanie pompy);
A1 - przekrój zbiornika 1;
A2 - przekrój zbiornika 2;
a1 - przekrój odpływu zbiornika 1;
a2 - przekrój odpływu zbiornika 2;
k - współczynnik skalujący;
g - grawitacja;
'''
timeplot1 = np.arange(0,2500,1)                                                                                         #dziedzina dla wykresu
timeplot2 = np.arange(0,7500,1)

t2 = np.arange(1,7500,1)


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #implementacja modelu systemu

u2_inter = interp1d(timeplot2,list_u2,kind='linear',fill_value='extrapolate')
g = 9.81 #m/s**2

def model(x,t,parametres):
    A1 = parametres[0]
    A2 = parametres[1]
    a1 = parametres[2]
    a2 = parametres[3]
    k = parametres[4]

    h1t = x[0]
    h2t = x[1]
    dh1dt = -((a1*np.sqrt(2*g))/A1)*np.sqrt(h1t) + 1/A1*k*u2_inter(t)
    dh2dt = -((a2*np.sqrt(2*g))/A2)*np.sqrt(h2t) + ((a1*np.sqrt(2*g))/A2)*np.sqrt(h1t)

    return dh1dt,dh2dt

y0 = [1,1]              #wartość startowa dla solvera równań różniczkowych


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #implementacja kwadratowego kryterium jakości

def kryterium(parametres):
    y = odeint(model,y0,t2,args=(parametres,))
    y2 = y[:,1]
    suma = 0
    for i in range(len(list_y2)-1):
        suma += (list_y2[i]-y2[i])**2
    print(suma)          #obecna wartość kryterium
    return suma

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #rozpoczęcie optymalizacji..
#optimum = 0
optimum = [27.8430, 27.6820, 1.3252, 4.2723,28.0373]                                                                     #wartość optimum po optymalizacji
                                                                                                                        #wykomentować w przypadku ponownej optymalizacji systemu
if optimum == False:
    x0 = [20, 10, 1, 21, 30]  # wartość startowa optymalizatora
    optimum = fmin(kryterium, x0)
    print(optimum)

u1_inter = interp1d(timeplot1,list_u1,kind='linear',fill_value='extrapolate')

def modeltestowy(x,t,parametres):
    A1 = parametres[0]
    A2 = parametres[1]
    a1 = parametres[2]
    a2 = parametres[3]
    k = parametres[4]

    h1t = x[0]
    h2t = x[1]
    dh1dt = -((a1*np.sqrt(2*g))/A1)*np.sqrt(h1t) + 1/A1*k*u1_inter(t)
    dh2dt = -((a2*np.sqrt(2*g))/A2)*np.sqrt(h2t) + ((a1*np.sqrt(2*g))/A2)*np.sqrt(h1t)

    return dh1dt,dh2dt

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' #prezentacja wyników

list_u1.pop(0)
list_u2.pop(7499)
print("\nWartość kryterium dla rozwiązania optymalnego..")
kryterium(optimum)
t1 = np.arange(1,2500,1)
z1 = odeint(modeltestowy,y0,t1,args=(optimum,))
z2 = odeint(model,y0,t2,args=(optimum,))

print("\nWspółczynniki korelacji; \nZbiór testowy:",r2_score(z1[:,1],list_u1),"\n")
print("\nZbiór treningowy:",r2_score(list_u2, z2[:,1]))

plt.figure()
plt.plot(t1,z1[:,1],c='r',label="estymacja")
plt.plot(timeplot1,list_y1,label="faktyczne pomiary")
plt.xlabel("t(j)")
plt.ylabel("Poziom powierzchni płynu w dolnym naczyniu")
plt.legend(loc='upper right')
plt.figure()
plt.plot(t2,z2[:,1],c='r',label="estymacja")
plt.plot(timeplot2,list_y2,c='b',label="faktyczne pomiary")
plt.xlabel("t(j)")
plt.ylabel("Poziom powierzchni płynu w dolnym naczyniu")
plt.legend(loc='upper right')

plt.show()