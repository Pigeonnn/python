#!/usr/bin/env python3

import os
import sys
import urllib.request
from bs4 import BeautifulSoup as BS
import numpy as np
import time

def slow_write(str):
    for x in str:
        sys.stdout.write(x)
        sys.stdout.flush()
        time.sleep(0.01)

def very_slow_write(str):
    for x in str:
        sys.stdout.write(x)
        sys.stdout.flush()
        time.sleep(0.1)

def get_random_ua():
    ua_file = 'D:/Work/repo_python/wykopper/ua_file'
    try:
        with open(ua_file) as f:
            lines = f.readlines()
        if len(lines) > 0:
            prng = np.random.RandomState()
            index = prng.permutation(len(lines) - 1)
            idx = np.asarray(index, dtype=np.integer)[0]
            random_proxy = lines[int(idx)]
    except Exception as ex:
        print('Cos nie pyklo w ua_file :/ .. :')
        print(str(ex))
    finally:
        return random_proxy

def get_pics(tag):
    user_agent = str(get_random_ua())
    user_agent = user_agent[:-1]
    headers = {
        'user-agent': user_agent,
    }
    print("\n.\n.\n.\n")
    slow_write("HEADER UŻYTY DO WYWOŁANIA REQUEST: \n")
    print(headers)
    slow_write("\nROZPOCZĘTO POBIERANIE GRAFIK Z TAGU: \n")
    very_slow_write(tag)
    print("\n")
    tag = str(tag)
    url = 'https://www.wykop.pl/tag/'+tag
    request = urllib.request.Request(url,headers=headers)
    wykop = urllib.request.urlopen(request)
    wykop_html = wykop.read()
    wykop.close()
    wykop_soup = BS(wykop_html, "html.parser")

    ClassCont = wykop_soup.findAll("div",{"class":"media-content"})

    PicsCont = []
    for img in ClassCont:
        Pic = (img.a['href']).split("imgurl=")
        PicStr = str(Pic)
        if PicStr[-5:-2] == 'jpg':
            PicsCont.append(PicStr[2:-2])
    counter = 0
    for img in PicsCont:
        print(img)
        FileName = str(np.random.randint(1,100000))
        urllib.request.urlretrieve(img,'C:/Users/Pigeon/Pictures/Saved Pictures/'+FileName+'.jpg')
        counter +=1
    return counter


os.system('cls')
width = os.get_terminal_size()
print("\n\n\n")
slow_write('WITAJ W INTERAKTYWNYM MENU ŚCIĄGANIA GRAFIKI Z WYKOPU ( ͡° ͜ʖ ͡°)\n')
input()
slow_write('Z JAKIEGO TAGU CHCIAŁBYŚ ŚCIĄGNĄĆ GRAFIKĘ?\n')
tag = str(input())
slow_write('ROZPOCZYNANIE POBIERANIA ...\n')
counter = get_pics(tag)
slow_write("\nŚciąganie obrazów zakończone. Całkowita ilość ściągniętych plików = ")
print(counter,"\n")